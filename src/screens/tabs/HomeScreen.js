import React, { Component } from 'react';
import { View, Text, StyleSheet, Alert, TouchableOpacity, ScrollView} from 'react-native';
import { Card, Button, Tile } from 'react-native-elements';
import moment from 'moment';
import { timingsByCity, getPrayerTime } from '../../services/AladhanApi';
import Icon from 'react-native-vector-icons/FontAwesome';

// Indonesian locale
var idLocale = require('moment/locale/id'); 
var newDate = new Date()
moment.locale('id', idLocale);

class HomeScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      name: '',
      date: '',
      timings: [],
      dateNow: '',
      prayers: {
        subuh: '', 
        dzuhur: '', 
        ashar: '', 
        maghrib: '', 
        isya: ''
      },
      schedule: {
        prayer: '',
        time: ''
      },
      cityName: 'KOTA JAKARTA',
      cityId: '667'
    }

    // var clock = null
    // update = () => {
    //   clock = moment(new Date()).format("HH:mm:ss")
    //   this.props.navigator.setSubTitle({subtitle: clock})
    // }
    // setInterval(update, 1000)
    var localDate = moment(newDate).format('dddd, DD MMM YYYY')
    this.props.navigator.setSubTitle({subtitle: localDate})
    
  }

  componentDidMount() {
    // timingsByCity('Jakarta').then(res => {
    //   this.setState({
    //     date: res.data.timings.Asr,
    //     timings: res.data.timings,
    //     dateNow: res.data.date.readable
    //   })
    // });
    this.loadPrayer()
    //this.handlePrayerTime()
  }

  loadPrayer = () => {
    // const newDate = new Date()
    const city = '667'
    const myDate = moment(newDate).format("YYYY-MM-DD")
    const myTime = moment(newDate).format("HH:mm")

    getPrayerTime(city, myDate).then(res => {
      console.log(res)
      const prayer = res.jadwal.data
      this.setState({
        prayers: {
          subuh: prayer.subuh, 
          dzuhur: prayer.dzuhur, 
          ashar: prayer.ashar, 
          maghrib: prayer.maghrib, 
          isya: prayer.isya,
        },
        dateNow: prayer.tanggal
      })
      console.log('ini '+this.state.prayers.subuh)
      this.handlePrayerTime()
      // if(myTime < prayer.maghrib){
      //   console.log('maghrib')
      //   this.setState({
      //     schedule: {
      //       prayer: 'Maghrib',
      //       time: prayer.maghrib
      //     }
      //   })
      // }
      // console.log(myTime+' vs '+prayer.maghrib)
      // console.log(myTime < prayer.maghrib ? 'iya' : 'ora')
      // console.log('ini '+this.state.schedule.prayer)
    }).catch(err => {
      console.log(err)
    })
  }

  handlePrayerTime = () => {
    // const newDate = new Date()
    const myTime = moment(newDate).format("HH:mm")
    console.log('subuh '+this.state.schedule.time)
    console.log(myTime+' vs '+this.state.prayers.subuh)

    if(myTime < this.state.prayers.subuh){
      console.log('subuh')
      this.setState({
        schedule: {
          prayer: 'Subuh',
          time: this.state.prayers.subuh
        }
      })
    } else if(myTime < this.state.prayers.dzuhur){
      console.log('dzuhur')
      this.setState({
        schedule: {
          prayer: 'Dzuhur',
          time: this.state.prayers.dzuhur
        }
      })
    }	else if(myTime < this.state.prayers.ashar){
      console.log('ashar')
      this.setState({
        schedule: {
          prayer: 'Ashar',
          time: this.state.prayers.ashar
        }
      })
    }	else if(myTime < this.state.prayers.maghrib){
      console.log('maghrib')
      this.setState({
        schedule: {
          prayer: 'Maghrib',
          time: this.state.prayers.maghrib
        }
      })
    } else if(myTime < this.state.prayers.isya){
      console.log('isya')
      this.setState({
        schedule: {
          prayer: 'Isya',
          time: this.state.prayers.isya
        }
      })
    } else if(myTime > this.state.prayers.subuh){
      console.log('subuh')
      this.setState({
        schedule: {
          prayer: 'Subuh',
          time: this.state.prayers.subuh
        }
      })
    }
    console.log('my time '+myTime)
  }

  clickCity = () => {
    this.props.navigator.showModal({
      screen: 'muslimApp.comp.SearchCity',
      title: 'Serach City',
      animationType: 'slide-up',
      passProps: {
        cityId: this.state.cityId,
        cityName: this.state.cityName,
        onUpdateCity: this._updateCity 
      },
      // navigatorStyle: {
      //   navBarCustomView: 'muslimApp.comp.SearchHeader'
      // },
    })
  }

  _updateCity = (name) => {
    this.setState({cityName: name})
  }
 
  render() {
    //const {latitude, longitude, error} = this.state;
    //console.log('mbut '+this.state.schedule.time)
    return (
      <ScrollView>
        <Card 
          title='Waktu Sholat'
          containerStyle={styles.cardHead}>
          <View style={{alignItems:'center', height: 120}} > 
            <Text style={{fontSize:25, fontWeight:'bold'}} >{this.state.schedule.prayer}</Text>
            <Text style={{fontSize:40, fontWeight:'bold', color:'#b71c1c'}} >{this.state.schedule.time}</Text>
            <TouchableOpacity onPress={this.clickCity} style={{flexDirection: 'row'}} >
              <Text style={{fontWeight: 'bold', padding: 10}}>{this.state.cityName}</Text>
              <Icon name="caret-down" size={18} style={{paddingTop:9}} />
            </TouchableOpacity>
          </View>
          {/* <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity>
              <Text style={{fontWeight: 'bold', padding: 10}}>Lihat semua</Text>
            </TouchableOpacity>
          </View> */}
        </Card>
        <View style={{flexDirection:'row', alignItems: 'center', justifyContent: 'center'}}>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  cardHead: {
    padding:10,
    
    margin:0,
    minHeight: 200,
    //justifyContent: 'center',
    //alignItems: 'center',
  }
});

export default HomeScreen;