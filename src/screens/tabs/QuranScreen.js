import React, { Component } from 'react';
import { View, Text, StyleSheet, ListView, AsyncStorage, FlatList } from 'react-native';
import { List, ListItem } from 'react-native-elements'
import { getChapter } from '../../services/QuaranApi';

class QuranScreen extends Component {

  static navigatorStyle = {
    navBarHideOnScroll: true,
    //drawUnderNavBar: true
  };

  constructor(props) {
    super(props);
    
    this.state = {
      chapters: []
    }
    this._renderItem = this._renderItem.bind(this);
    
  }

  componentDidMount() {
    AsyncStorage.getItem('chapter').then(ret => {
      if(ret === null){
        getChapter().then(res => {
          if(res.chapters){
            this.setState({
              chapters: res.chapters
            })
            AsyncStorage.setItem('chapter', JSON.stringify(res.chapters))
          }
        })
      }else{
        this.setState({
          chapters: JSON.parse(ret)
        })
        this.props.navigator.setSubTitle({subtitle: `${Object.keys(JSON.parse(ret)).length} Surah`});
        //console.log(Object.keys(JSON.parse(ret)).length)
      }
    })   
  }

  onPressItem = (noChapter, nameChapter, totalVerses) => {
    this.props.navigator.push({
      screen: 'muslimApp.ReadQuranScreen',
      title: 'kokoko',
      navigatorStyle: {
        tabBarHidden: true,
        expendCollapsingToolBarOnTopTabChange: false,
        navBarHideOnScroll: true,
        drawUnderTabBar: true
      },
      passProps: {
        _numberChapter: noChapter,
        _nameChapter: nameChapter,
        _totalVerses: totalVerses 
      }
    });
  }

  _renderItem({item}){
    return(
      <ListItem
        key={item.id}
        title={item.name_simple}
        titleStyle={styles.title}
        subtitle={`${item.verses_count} Ayat`}
        rightTitle={item.name_arabic}
        rightTitleStyle={styles.titleRight}
        onPress={() => {this.onPressItem(item.id, item.name_simple, item.verses_count)}}
      />
    )
  }

  render() {
    return (
      <View style={styles.container} >
      <FlatList
        data={this.state.chapters}
        extraData={this.state}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: 'white',
  },
  title: {
    fontSize: 16
  },
  titleRight: {
    fontSize: 19,
    color: 'black'
  }
});

export default QuranScreen;
