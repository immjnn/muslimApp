//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

// create a component
class FavoriteScreen extends Component {

  onPressButton = () => {
    this.props.navigator.push({
      screen: 'muslimApp.PushedScreen',
      title: 'Pushed Screen',
      navigatorStyle: {tabBarHidden: true}
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Favorite Screen</Text>
        <Button title="Press Me!" onPress={() => this.onPressButton()} />
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});

//make this component available to the app
export default FavoriteScreen;
