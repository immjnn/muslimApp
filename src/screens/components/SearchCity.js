//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableNativeFeedback, Keyboard } from 'react-native';

import { getCities } from '../../services/AladhanApi'

// create a component
class SearchCity extends Component {

  
  // static navigatorStyle = {
  //   drawUnderNavBar: true,
  //   navBarCustomView: 'muslimApp.comp.SearchHeader',
  //   navBarCustomViewInitialProps: {
  //     cityId: 'this.props.cityId',
  //     cityName: 'this.props.cityName'
  //   }
  // }

  constructor(props){
    super(props);
    this.state = {
      cities: [],
      cities2: [],
      noData: false,
    }
    console.log(this.props)
    this.props.navigator.setStyle({
      navBarCustomView: 'muslimApp.comp.SearchHeader',
      navBarCustomViewInitialProps: {
        cityId: this.props.cityId,
        cityName: this.props.cityName,
        onSearchText: this.searchText
      }
    });
  }

  componentDidMount() {
    this.loadCities();
  }

  loadCities = () => {
    getCities().then(res => {
      const data = res.kota
      this.setState({cities: data, cities2: data})
      //console.log(res)
    }).catch(err => {
      console.log(err)
    })
  }

  onPressCity = (name) => {
    //alert(name)
    this.props.onUpdateCity(name)
    this.props.navigator.dismissModal({
      animationType: 'slide-down'
    });
    Keyboard.dismiss()
  }

  searchText = (e) => {
    let text = e.toLowerCase()
    let city = this.state.cities2
    let filteredName = city.filter((item) => {
      return item.nama.toLowerCase().match(text)
    })
    if (Array.isArray(filteredName)) {
      console.log(filteredName)
      if(!filteredName.length){
        this.setState({
          noData: true
        })
      }else{
        this.setState({
          noData: false,
          cities: filteredName
        })
      }
    }
  }

  _renderItem = ({item}) => (
    <TouchableNativeFeedback useForeground={true} onPress={() => this.onPressCity(item.nama)}>
      <View style={styles.container}>
          <Text style={{fontSize: 17}}>{item.nama}</Text>
      </View>
    </TouchableNativeFeedback>
  )

  render() {
    //console.log(this.state.noData)
    if(this.state.noData){
      return (
        <View style={{flex:1, backgroundColor:'#ffffff', alignItems: 'center',}}>
          <Text>No Data</Text>
        </View>
      )
    }
    return (
      <FlatList
        style={{flex:1, backgroundColor: '#ffffff'}}
        data={this.state.cities}
        extraData={this.state}
        keyExtractor={(item) => item.id}
        renderItem={this._renderItem}
        keyboardShouldPersistTaps='always'
      />
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    //fontSize: 20,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#ffffff',
    padding: 5
  },
});

//make this component available to the app
export default SearchCity;
