import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Button, TextInput, Slider, Switch, TouchableOpacity } from 'react-native';

class SettingQuran extends Component {
  constructor(props){
    super(props);
    this.state = {
      font: this.props.font,
      showTranslation: this.props.showTranslation
    }
  }

  _saveFont = (_font) => {
    this.props.onSaveFont(_font)
    this.setState({font: _font})
  }

  _hideTrans = (_hide) => {
    console.log(this.state.showTranslation)
    this.props.onShowTrans(_hide)
    this.setState({showTranslation: _hide})
  }
  
  _saveSettings(){
    
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{flex: 4}}>
          <View style={styles.headerContainer}>
            <Text style={styles.title}>{this.props.title}</Text>
            <TouchableOpacity onPress={() => this._saveSettings()}>
              <Text style={styles.title}>Save</Text>
            </TouchableOpacity>
          </View>
          
          <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingTop:10}}>
            <Text style={styles.content}>Ukuran teks</Text>
            <Text style={{marginTop:7, color: '#b71c1c'}} >{this.state.font}</Text>
          </View>
          <Slider 
            step={1}
            maximumValue={40}
            minimumValue={10}
            value={this.props.font}
            onValueChange= {(font) => this._saveFont(font)}
            style={styles.containerSlider}
          />
          <View style={{flexDirection: 'row', marginTop:10, justifyContent: 'space-between'}}>
            <Text style={styles.content}>Tampilkan terjemahan </Text>
            <Switch 
              style={{paddingTop:10}}
              value={this.state.showTranslation}
              onValueChange={(show) => this._hideTrans(show)}
            />
          </View>
          
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width * 0.8,
    height: Dimensions.get('window').height * 0.3,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    padding: 10,
  },
  title: {
    fontSize: 17,
    fontWeight: '700',
    paddingBottom: 5,
    
  },
  content: {
    marginTop: 7,
    color: 'black'
  },
  containerSlider: {
    margin:0,
    padding:0
  },
  headerContainer: {
    flexDirection: 'row', 
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: '#ddd',
  }
});

export default SettingQuran;