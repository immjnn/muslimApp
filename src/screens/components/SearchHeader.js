//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';

// create a component
class SearchHeader extends Component {

  handleSearch = (name) => {
    this.props.onSearchText(name)
  }

  render() {
    console.log(this.props)
    return (
      <View style={{paddingTop:5}}>
        <TextInput 
          style={{fontSize: 17}}
          placeholder='Cari Kota' 
          underlineColorAndroid='transparent'
          onChangeText={(name) => this.handleSearch(name)}
          autoCapitalize='characters'
          autoFocus={true}
        />
      </View>
    );
  }
}

//make this component available to the app
export default SearchHeader;
