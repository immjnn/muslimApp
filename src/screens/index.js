import { Navigation } from 'react-native-navigation';

import HomeScreen from './tabs/HomeScreen';
import FavoriteScreen from './tabs/FavoriteScreen';
import QuranScreen from './tabs/QuranScreen';
import QiblaScreen from './tabs/QiblaScreen';
import PushedScreen from './stacks/PushedScreen';
import ReadQuranScreen from './stacks/ReadQuranScreen';
import SettingQuran from './components/SettingQuran';
import SearchHeader from './components/SearchHeader';
import SearchCity from './components/SearchCity';

// register all screens of the app (including internal ones)
export function registerScreens() {
  Navigation.registerComponent('muslimApp.HomeScreen', () => HomeScreen);
  Navigation.registerComponent('muslimApp.FavoriteScreen', () => FavoriteScreen);
  Navigation.registerComponent('muslimApp.QuranScreen', () => QuranScreen);
  Navigation.registerComponent('muslimApp.QiblaScreen', () => QiblaScreen);
  Navigation.registerComponent('muslimApp.PushedScreen', () => PushedScreen);
  Navigation.registerComponent('muslimApp.ReadQuranScreen', () => ReadQuranScreen);
  Navigation.registerComponent('muslimApp.comp.SettingQuran', () => SettingQuran);
  Navigation.registerComponent('muslimApp.comp.SearchHeader', () => SearchHeader);
  Navigation.registerComponent('muslimApp.comp.SearchCity', () => SearchCity);
}