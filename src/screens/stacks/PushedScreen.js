//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { ListItem } from 'react-native-elements';
import { getBySurah } from '../../services/QuaranApi';

// create a component
class PushedScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      englishName: '',
      ayahs: [],
    }
  }

  componentWillMount() {
    getBySurah(this.props._numberSurah).then(res => {
      this.setState({
        englishName: res.data.englishName,
        ayahs: res.data.ayahs
      })
    })
  }

  _renderItem(item){
    return(
      <ListItem 
        titleStyle={styles.textCus} 
        titleNumberOfLines={2} 
        title={item.text} 
        key={item.number} 
      />
    )
  }

  render() {
    //console.log(this.props)
    return (
      // <View style={styles.container}>
      //   <Text style={styles.textCus} >{this.state.englishName}</Text>
      //   {this.state.ayahs.map((l, index) => {
      //     return <Text style={styles.textCus}>{l.text}</Text>
      //   })}
      // </View>
      <FlatList
        data={this.state.ayahs}
        keyExtractor={(item, index) => index}
        renderItem={({item}) => this._renderItem(item)}
      />
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  textCus: {
    color: 'black',
    fontSize: 25,
  }
});

//make this component available to the app
export default PushedScreen;
