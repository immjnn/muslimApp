import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, AsyncStorage, Button, Platform } from 'react-native';
import { getQuranByChapter } from '../../services/QuaranApi';
import { iconsMap, iconsLoaded } from '../../components/MyIcons';

class ReadQuranScreen extends Component {

  static navigatorStyle = {
    navBarHideOnScroll: true,
    //drawUnderNavBar: true
  };

  constructor(props){
    super(props);
    this.state = {
      verses: [],
      font: 30,
      showTranslation: true
    }

    console.log(this.props.navigator)
    this.props.navigator.setTitle({title: this.props._nameChapter})
    this.props.navigator.setSubTitle({subtitle: `${this.props._totalVerses} Ayat`})
    this._renderItem = this._renderItem.bind(this)

    this.props.navigator.setOnNavigatorEvent(this.onPressSetting.bind(this));
    iconsLoaded.then(() => {
      this.props.navigator.setButtons({
        rightButtons: [
          {
            id: 'setting',
            title: 'Set',
            icon: iconsMap['ios-settings']
          }
        ]
      })
    })
  }

  componentDidMount() {
    AsyncStorage.getItem(this.props._nameChapter).then(ret => {
      if(ret === null){
        getQuranByChapter(this.props._numberChapter, this.props._totalVerses).then(res => {
          if(res.verses){
            this.setState({
              verses: res.verses
            })
            AsyncStorage.setItem(this.props._nameChapter, JSON.stringify(res.verses))
          }
        })
      }else{
        this.setState({
          verses: JSON.parse(ret)
        })
      }
    })  
  }

  showLightBox = () => {
    this.props.navigator.showLightBox({
      screen: "muslimApp.comp.SettingQuran",
      passProps: {
        title: 'Setting',
        font: this.state.font,
        showTranslation: this.state.showTranslation,
        onClose: this.dismissLightBox,
        onSaveFont: this.saveFont,
        onShowTrans: this.showTrans
      },
      style: {
        backgroundBlur: 'dark',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        tapBackgroundToDismiss: false
      }
    });
  }

  dismissLightBox = () => {
    this.props.navigator.dismissLightBox();
  }

  saveFont = (setFont) => {
    this.setState({font: parseInt(setFont)})
  }

  showTrans = (show) => {
    this.setState({showTranslation: show})
  }

  onPressSetting(event) {
    if(event.type == 'NavBarButtonPress'){
      if(event.id == 'setting'){
        this.showLightBox()
      }
    }
  }

  _renderItem = ({item}) => {
    return (
      <View style={styles.viewContainer} > 
        <Text style={{fontSize: this.state.font, color:'black', padding:2, lineHeight: 50,}} >{item.text_indopak}</Text>
        <View style={{flexDirection: 'row'}}>
          <Text style={{fontSize:15, color:'#b71c1c', paddingRight:5, fontWeight:'bold'}}>{item.verse_number}</Text>
          {
            !this.state.showTranslation ? null :
            <Text style={{fontSize:15, color:'#424242', marginRight:10}}>{item.translations[0].text}</Text>
          }
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container} >
        <FlatList 
          initialNumToRender={50}
          maxToRenderPerBatch={100}
          data={this.state.verses}
          extraData={this.state}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  viewContainer: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5,
    padding: 5,
    backgroundColor: 'white'
  },
});

export default ReadQuranScreen;
