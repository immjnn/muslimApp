import React from 'react';
import { Navigation } from 'react-native-navigation';
import { iconsMap, iconsLoaded } from './components/MyIcons';

import { registerScreens } from './screens';

registerScreens(); // this is where you register all of your app's screens

iconsLoaded.then(() => {
  startApp();
});

function startApp() {
  // start the app
  Navigation.startTabBasedApp({
    tabs: [
      {
        label: 'Home',
        screen: 'muslimApp.HomeScreen', // this is a registered name for a screen
        icon: iconsMap['ios-home'],
        selectedIcon: iconsMap['ios-home--active'], // iOS only
        title: 'Home'
      },
      {
        label: 'Al-Quran',
        screen: 'muslimApp.QuranScreen',
        icon: iconsMap['ios-book-outline'],
        selectedIcon: iconsMap['ios-book-outline--active'], // iOS only
        title: 'Al-Quran'
      },
      {
        label: 'Favorite',
        screen: 'muslimApp.FavoriteScreen',
        icon: iconsMap['ios-star'],
        selectedIcon: iconsMap['ios-star--active'], // iOS only
        title: 'Favorite'
      },
      {
        label: 'Kiblat',
        screen: 'muslimApp.QiblaScreen',
        icon: iconsMap['ios-compass'],
        selectedIcon: iconsMap['ios-compass--active'], // iOS only
        title: 'Kiblat'
      }
    ],
    appStyle: {
      tabBarBackgroundColor: '#ffffff',
      tabBarButtonColor: '#424242',
      tabBarSelectedButtonColor: '#b71c1c',
      tabBarTranslucent: true,
      statusBarTextColorScheme: 'dark',
      statusBarColor: '#fafafa',
      navBarBackgroundColor: '#ffffff',
      forceTitlesDisplay: true,
    },
    animationType: 'fade'
  });
}


