import React from 'react';

const host = 'http://staging.quran.com:3000/api/v3';

export async function getChapter() {
  try{
    let response = await fetch(`${host}/chapters?language=id`);
    return response.json();
  } catch(error){
    console.log(error)
  }
}

export async function getQuranByChapter(number, limit=500) {
  try{
    let response = await fetch(`${host}/chapters/${number}/verses?recitation=1&translations=33&language=id&limit=${limit}&text_type=words`);
    return response.json();
  } catch(error){
    console.log(error)
  }
}