import React from 'react';

const host = 'http://api.aladhan.com';

// new host API
const host2 = 'https://api.fathimah.xyz/sholat/format/json';

export async function timingsByCity(city) {
  try{
    let response = await fetch(`${host}/timingsByCity?city=${city}&country=Indonesia&method=11`);
    return response.json();
  } catch(error){
    console.log(error)
  }
}

export async function getPrayerTime(city, date) {
  try{
    let response = await fetch(`${host2}/jadwal/kota/${city}/tanggal/${date}`);
    return response.json();
  } catch(error){
    console.log(error)
  }
}

export async function getCities() {
  try{
    let response = await fetch(`${host2}/kota`);
    return response.json();
  } catch(error){
    console.log(error)
  }
}