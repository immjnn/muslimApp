package com.muslimapp;

import android.widget.LinearLayout;
import android.graphics.Color;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.Gravity;
import android.util.TypedValue;
import com.muslimapp.R;

import com.reactnativenavigation.controllers.SplashActivity;

public class MainActivity extends SplashActivity {

    @Override
    public LinearLayout createSplashLayout() {
        LinearLayout view = new LinearLayout(this);
        TextView textView = new TextView(this);
        ImageView img = new ImageView(this);

        view.setBackgroundColor(Color.parseColor("#ffffff"));
        view.setGravity(Gravity.CENTER);

        img.setImageResource(R.drawable.splash);

        // textView.setTextColor(Color.parseColor("#b71c1c"));
        // textView.setText("Al-Quran");
        // textView.setGravity(Gravity.CENTER);
        // textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 40);

        // view.addView(textView);
        view.addView(img);
        return view;
    }
}